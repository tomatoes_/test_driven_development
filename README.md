# Test Driven Development #

### Zadanie ###

Utworzyć konsolową aplikację dla restauracji.

Aplikacja ma wyświetlać ofertę użytkownikowi (menu -> kategorie -> ...) i prowadzić z nim interakcję (użytkownik wybiera danie wpisując np. jego numer).

Do każdej klasy zawierającej logikę **najpierw piszemy przypadki testowe i testy**. Zależności (takie jak interakcja z użytkownikiem) **mockujemy**.
Wymagania:

1. w aplikacji tworzymy klasę menu (taka zmockowana baza danych z hardcodowanymi polami)
2. aplikacja musi reagować na błędny ID
3. aplikacja musi zapisywać dane zamówienia i tworzyć na ich podstawie paragon (wyświetlać)