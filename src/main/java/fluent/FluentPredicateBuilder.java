package fluent;

import com.google.common.collect.Maps.EntryTransformer;
import lombok.RequiredArgsConstructor;

import java.util.Objects;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

import static java.util.Objects.*;
import static java.util.function.Function.identity;
import static lombok.AccessLevel.PROTECTED;

@RequiredArgsConstructor(access = PROTECTED)
public class FluentPredicateBuilder<T, Mapped> {

    protected static final Consumer<Throwable> EMPTY_FALLBACK = t -> {};

    private final Function<? super T, ? extends Mapped> mappingFunction;
    private final Consumer<Throwable> lastFallback;

    public static <T> FluentPredicateBuilder<T, T> is() {
        return new FluentPredicateBuilder<>(identity(), EMPTY_FALLBACK);
    }

    public static <T, Mapped> FluentPredicateBuilder<T, Mapped> is(Function<? super T, ? extends Mapped> mappingFunction) {
        return is(mappingFunction, EMPTY_FALLBACK);
    }

    public static <T, Mapped> FluentPredicateBuilder<T, Mapped> is(Function<? super T, ? extends Mapped> mappingFunction,
                                                                   Consumer<Throwable> fallback) {
        try {
            return new FluentPredicateBuilder<T, Mapped>(mappingFunction, fallback);
        } catch (Exception throwable) {
            fallback.accept(throwable);
            return new NullFluentPredicateBuilder<T, Mapped>();
        }
    }

    public <NewMapped> FluentPredicateBuilder<T, NewMapped> mappedTo(Function<? super Mapped, ? extends NewMapped> mappingFunction) {
        return mappedTo(mappingFunction, lastFallback);
    }

    public <NewMapped> FluentPredicateBuilder<T, NewMapped> mappedTo(Function<? super Mapped, ? extends NewMapped> mappingFunction,
                                                                     Consumer<Throwable> fallback) {
        try {
            return is(t -> mappingFunction.apply(mapped(t)), fallback);
        } catch (Exception throwable) {
            fallback.accept(throwable);
            return new NullFluentPredicateBuilder<T, NewMapped>();
        }
    }

    public FluentPredicate equalTo(Mapped other) {
        return new FluentPredicate(other, Objects::equals, Stream<Mapped>::anyMatch);
    }

    public FluentPredicate notEqualTo(Mapped other) {
        return new FluentPredicate(other, Objects::equals, Stream<Mapped>::noneMatch);
    }

    public class FluentPredicate implements Predicate<T> {
        private Queue<Mapped> objects = new LinkedBlockingQueue<>();
        private BiFunction<? super Mapped, ? super Mapped, Boolean> predicateFunction;
        private EntryTransformer<Stream, Predicate<? super Mapped>, Boolean> transformer;

        private FluentPredicate(Mapped other, BiFunction<? super Mapped, ? super Mapped, Boolean> predicateFunction,
                                EntryTransformer<Stream, Predicate<? super Mapped>, Boolean> transformer) {
            this.predicateFunction = predicateFunction;
            this.transformer = transformer;
            or(other);
        }

        public FluentPredicate or(Mapped other) {
            objects.offer(other);
            return this;
        }

        public boolean test(T object) {
            return transformer.transformEntry(objects.stream(), o -> predicateFunction.apply(mapped(object), o));
        }
    }

    public class NullFluentPredicate extends FluentPredicate {
        protected NullFluentPredicate(Mapped other) {
            super(other, null, null);
        }

        @Override
        public boolean test(T object) {
            return isNull(object);
        }
    }

    private Mapped mapped(T object) {
        if (isNull(mappingFunction)) {
            return null;
        }
        return mappingFunction.apply(object);
    }
}

