package fluent;

import java.util.function.Consumer;
import java.util.function.Function;

public class NullFluentPredicateBuilder<T, Mapped> extends FluentPredicateBuilder<T, Mapped> {

    protected NullFluentPredicateBuilder() {
        super(null, EMPTY_FALLBACK);
    }

    @Override
    public <NewMapped> FluentPredicateBuilder<T, NewMapped> mappedTo(Function<? super Mapped, ? extends NewMapped> mappingFunction) {
        return new NullFluentPredicateBuilder<>();
    }

    @Override
    public <NewMapped> FluentPredicateBuilder<T, NewMapped> mappedTo(Function<? super Mapped, ? extends NewMapped> mappingFunction, Consumer<Throwable> fallback) {
        return new NullFluentPredicateBuilder<>();
    }

    @Override
    public FluentPredicate equalTo(Mapped other) {
        return new NullFluentPredicate(other);
    }

    @Override
    public FluentPredicate notEqualTo(Mapped other) {
        return new NullFluentPredicate(other);
    }
}

